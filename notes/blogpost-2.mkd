Context²
-------

Recently, we have been introduced by our friend Seda to her colleagues at the department of crypto in KU Leuven to work on their visual identity. About 60 people work there, on topics ranging from low level mathematics to privacy in social networks. It was for us the perfect job to investigate the possibilities of thinking of visual identity as a programme as opposed to a more classical static approach.

There were only a few stipulations regarding the design, which came from an initial meeting some months ago. The most stern was: NO KEYS! That is, they did not want any cheap and inapplicable metaphors for computer security. So, no keys, no locks, and also no silly streams of 0s and 1s in glossy compsec green.

Approaches
----------

So an email was sent out to the OSP troops: would anyone like to participate? In a rare change of form, STDIN (Steph and Alex) would not be working together. Who could handle working with Alex by himself? Only one person seemed brave enough ;)

(This concern was later erased by the introduction of STDIN's first, and greatest, intern, Eduard).

On a rainy Wednesday John arrived in Brussels to begin experimenting with Alex on possible approaches. Browsing through Google Image searches on cryptography for inspiration, we did not find any easy answers. Sure, [crypto wheels](http://www.google.com/search?q=crypto+wheel&um=1&ie=UTF-8&tbm=isch&source=og&sa=N&hl=en&tab=wi&biw=1252&bih=520) are not keys, but are they any less inappropriate and anachronistic?

It was then that two ideas came together. The first was: how do we even _create_ a generative logo? What format could even support it?

The answer was PostScript. PostScript is a [Turing complete](http://en.wikipedia.org/wiki/Turing_complete), meaning that it is ready and able to program, well, _anything_. There are PostScript files which were programmed to crash your printer and spread like a virus. Support for PostScript through the Encapsulated PostScript (EPS) format is relatively widespread (even is Microsoft has chosen an inferior and annoying interpreter for their Office products). The prospect of creating a single graphics file which embedded its own generativity was too much to resist.

The second idea was related to somehow expressing crucial aspects of cryptography in a way that avoided all the old cliches. Perhaps inspired by our first experiment in "random" PostScript output, `randlines.eps`, Alex's mind grepped and returned (through the STDOUT of his STDIN) with a breakthrough: Moire patterns! 

Moire + PostScript == &lt;3 &lt;3 &lt;3
---------------------------------------

And so began the experimentation. Early forays into the language proved invigorating. A programming language that is at once dynamic and interpreted (just like those fancy and once-derided "scripting" languages) _and_ stack-based (like, um, [assembly](http://en.wikipedia.org/wiki/Assembly_language))??

Bring it on!

So while John worked at parsing and producing code in this historically alien hybrid, Alex and the new super-intern Edu began extensive visual testing in Inkscape. Though many have not yet been reinterpreted as PostScript code (because, and probably rightly so, Inkscape's Cairo-based PostScript export is concerned with exact representation and not cool, malleable loop based drawing), they continue to provide inspiration. One day soon we hope to be literate enough to reproduce them in PostScript code.

{an example jpg or more goes here}

Though extremely simple, the following image shows an initial excursion into logo generation. It features an embedded copy of [Lato Black](http://www.google.com/webfonts/specimen/Lato) that was subsetted to include only the necessary letters ('c', 'o', 's', and 'i') and then converted to PostScript Type 42 to facilitate direct embedding in the EPS file.[^t42] The dots are generated through `for` loops and simple PostScript operators.

[^t42]: This process will be outlined in a later post.

{pattern.eps}


embeddable, self-contained.

moire effect and crypto
relation between

tex, metafont and latin modern

letterhead, templates for presentations and bussiness card


LINKS
-----
- [Why Not Smile - trippy Moire videos and (also interesting design)](http://whynotsmile.com/project/transitional.html)
- [http://www.youtube.com/watch?v=wGjmwyoMirc](http://www.youtube.com/watch?v=wGjmwyoMirc)
- <http://www.gametheory.ch/index.jsp?positionId=101450>
- <http://kai.jauslin.biz/personal/semantic-development/>
- <http://kai.jauslin.biz/other/visual-cryptography/>
- <http://www.munart.org/index.php?p5=1>
- <http://rijmenants.blogspot.com/2008/01/visual-cryptography.html>

